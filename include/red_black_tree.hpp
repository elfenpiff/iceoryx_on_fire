#pragma once

#include <cstdint>
#include <iostream>
#include <limits>
#include <new>
#include <stack>

// https://algorithmtutor.com/Data-Structures/Tree/Red-Black-Trees/
// https://www.programiz.com/dsa/red-black-tree

namespace iox
{
namespace cxx
{
template <typename Type, uint64_t Capacity>
class red_black_tree
{
  public:
    red_black_tree();

    bool erase(const Type& value);
    bool insert(const Type& value);

  private:
    enum class Color : uint8_t
    {
        RED = 0,
        BLACK = 1
    };

    struct Node
    {
        bool has_left_substree() const
        {
            return left != INVALID_INDEX;
        }
        bool has_right_substree() const
        {
            return right != INVALID_INDEX;
        }
        bool has_parent() const
        {
            return parent != INVALID_INDEX;
        }

        uint64_t parent = INVALID_INDEX;
        uint64_t left = INVALID_INDEX;
        uint64_t right = INVALID_INDEX;
        Type value;
        Color color = Color::BLACK;
    };

    void left_rotate(const uint64_t index);
    void right_rotate(const uint64_t index);

    Node& get_node(const uint64_t index);
    void insert_fix(Node& newNode);

  private:
    static constexpr uint64_t INVALID_INDEX = std::numeric_limits<uint64_t>::max();

    using NodeMemory = uint8_t[sizeof(Node)];

    alignas(alignof(Node)) NodeMemory m_nodes[Capacity];
    uint64_t m_root = INVALID_INDEX;

    std::stack<uint64_t> m_indices;
};
} // namespace cxx
} // namespace iox

#include "red_black_tree.inl"
